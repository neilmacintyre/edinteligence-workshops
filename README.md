# Workshop/Disscusion groups

## Virtual workshop ideas:
- A simple way to share the workshops would be through git
  - Branches could be used to share different blocks of the project (ie part 1, part2 ...). This would be an effective way to keep a whole group of people on the same page.
    
## Workshop topic Ideas:
- Entropy and Information theory: concepts regarding entropy seem to pop up a lot in ML research papers. Generally speaking with a background in probablity theory a high level overview of the topic would be achieveable.
- Manifold Hypothesis

## Thoughts on Notebook Format:
- A pedalogical comment: A common shortcoming of notebooks I have used as learning materials is that they be "completed" without actually thinking about the concepts they convey. This not only hinders learning but makes it hard to assess how much you really have learned which can demotivating. In order to combat this I propose the following format of notebooks we should be along the lines of the following
  ### - Give a Question:
    - Eg. We saw how the model deformed space like this [ insert picture ]. Is this deformation stable in the sense that no matter random inital weight are choosen SGD will reach the same transformation?

  ### - Ask for a hypothesis: 
    - Eg. No 

  ### - Consider ways to test hypothesis
    - In the case of "No" hypothesis
      - Eg. Brute force random variable
      - Eg. Reverse engieer from a different transformation that result in a  linearly seperable line.
    - In the case of "Yes" hypothesis
      - To be 100% rigoourous require a maths proof
      - Brute forcish : Start with a bunch of different transforms and show that all of the transform approach the given one
      
  ### Implement a small sample test 

  ### Provide answer and explination 

  ### Address Probable Misconceptions

  ### Ask to connect to outside problems

  ### Provide connection to theory and applications
    - Eg. this is related to the local optimum choosen reached by the SGD. 
    - Eg. ?The manifold hyptheise gives an perspective images created by deep fakes GANS -- the GAN learns the face manifold to sample from ?
      - [Voxel-Based Variational Autoencoders - YouTube](https://www.youtube.com/watch?v=LtpU1yBStlU)